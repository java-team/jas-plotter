#!/bin/sh -e

set -x

VERSION=$2
DOWNLOADED_FILE=$3
PACKAGE=$(dpkg-parsechangelog | sed -n 's/^Source: //p')
TAR=../${PACKAGE}_${VERSION}+dfsg.orig.tar.gz
DIR=${PACKAGE}-${VERSION}

svn export svn://svn.freehep.org/svn/freehep/tags/$PACKAGE-$VERSION $DIR
rm -fr $DIR/src/main/java/tablelayout
rm -f $DIR/src/main/java/jas/util/layout/PnutsLayout.java
rm -f $DIR/src/main/java/jas/util/xml/XMLCharacterProperties.java
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR

rm -f $DOWNLOADED_FILE

